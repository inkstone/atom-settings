Export list of installed packages:
apm list --installed --bare > ~/.atom/package.list

Import packages:
apm install --packages-file ~/.atom/package.list
